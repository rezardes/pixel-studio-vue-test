const state = {
    activeTodo: {},
    dialog: false,
    isLoading: false
};

const getters = {
    getActiveTodo: state => state.activeTodo,
    getDialog: state => state.dialog,
    getIsLoading: state => state.isLoading
};

const actions = {
    
    enableDialog({ commit }) {
        commit('setDialogTrue')
    },

    disableDialog({ commit }) {
        commit('setDialogFalse')
    },

    updateTitle({ commit }, title) {
        commit('setTitle', title)
    },

    putActiveTodo({ commit }, todo) {
        commit('setActiveTodo', todo)
    },

    toggleLoadingBar({ commit }) {
        commit('toggleLoading')
    }
};

const mutations = {
    setDialogTrue: (state) => (state.dialog = true),
    setDialogFalse: (state) => (state.dialog = false),
    setTitle: (state, title) => (state.activeTodo.title = title),
    setActiveTodo: (state, todo) => (state.activeTodo = {...todo}),
    toggleLoading: (state) => (state.isLoading = (state.isLoading) ? false : true)
};

export default {
    state,
    getters,
    actions,
    mutations
};