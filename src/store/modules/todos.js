import axios from "axios"
import { isEnabledCreateEndpoint, isEnabledDeleteEndpoint, isEnabledUpdateEndpoint }  from "../../settings"

const state = {
    completed: [],
    uncompleted: [],
    activeTodo: {}
};

const getters = {
    getCompleted: state => state.completed,
    getUncompleted: state => state.uncompleted
};

const actions = {
    async fetchTodos({ commit }, isCompleted = false) {
        const response = await axios.get(
            'http://jsonplaceholder.typicode.com/todos'
        )

        commit('setTodos', {"data": response.data, isCompleted})
    },

    async createTodo({ commit }, title) {
        console.log('create todo loading...')
        var data = {title, completed: false}

        if (isEnabledCreateEndpoint) {
            const response = await axios.post(
                'http://jsonplaceholder.typicode.com/todos', 
                data
            )
            data = response.data
        }
        
        console.log('create finished...')
        commit('addTodo', data)
    },

    async removeTodo({ commit }, todo) {
        
        console.log('delete todo loading...')
        
        if (isEnabledDeleteEndpoint) {
            await axios.delete(
                `https://jsonplaceholder.typicode.com/todos/${todo.id}`
            )
        }
        
        console.log('delete finished...')
        commit('removeTodo', todo)
    },

    async updateTodo({ commit }, todo) {
        console.log('update todo loading...')

        if (isEnabledUpdateEndpoint) {
            const response = await axios.put(
                `http://jsonplaceholder.typicode.com/todos/${todo.id}`,
                todo
            )
            todo = response.data
        }
        
        console.log('update finished...')
        commit('modifyTodo', todo)
    },

    async setComplete({ commit }, todo) {
        console.log('update todo loading...')
        
        if (isEnabledUpdateEndpoint) {
            const response = await axios.put(
                `http://jsonplaceholder.typicode.com/todos/${todo.id}`,
                todo
            )
            todo = response.data
        }

        todo.completed = true
        console.log('update finished...')
        commit('moveToCompleted', todo)
    },

    async setUncomplete({ commit }, todo) {
        console.log('update todo loading...')

        if (isEnabledUpdateEndpoint) {
            const response = await axios.put(
                `http://jsonplaceholder.typicode.com/todos/${todo.id}`,
                todo
            )
            todo = response.data
        }

        todo.completed = false
        console.log('update finished...')
        commit('moveToUncompleted', todo)
    }
};

const mutations = {
    setTodos: (state, data) => {
        var result = data.data.filter(todo => todo.completed == data.isCompleted)
        if (data.isCompleted) {
            state.completed = result
        } else {
            state.uncompleted = result
        }
    },
    addTodo: (state, todo) => {state.uncompleted.unshift(todo)},
    removeTodo: (state, data) => {
        if (data.completed) {
            state.completed = state.completed.filter(todo => todo.id !== data.id)
        } else {
            state.uncompleted = state.uncompleted.filter(todo => todo.id !== data.id)
        }
    },
    modifyTodo: (state, data) => {
        
        var todoState = null
        if (data.completed) {
            todoState = state.completed
        } else {
            todoState = state.uncompleted
        }

        var index = todoState.findIndex(todo => todo.id === data.id)
        
        if (index !== -1) {
            todoState.splice(index, 1, data)
        }
    },
    moveToUncompleted: (state, data) => {
        state.completed = state.completed.filter(todo => todo.id !== data.id)
        state.uncompleted.unshift(data)
    },
    moveToCompleted: (state, data) => {
        state.uncompleted = state.uncompleted.filter(todo => todo.id !== data.id)
        state.completed.unshift(data)
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};