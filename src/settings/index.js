export const isEnabledCreateEndpoint = process.env.VUE_APP_CREATE_ENDPOINT_ENABLED === 'true'
export const isEnabledUpdateEndpoint = process.env.VUE_APP_UPDATE_ENDPOINT_ENABLED === 'true'
export const isEnabledDeleteEndpoint = process.env.VUE_APP_DELETE_ENDPOINT_ENABLED === 'true'